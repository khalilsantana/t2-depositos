#include <omp.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <string>

int n_deposits_max = 5000;
int ammount_per_deposit = 1;
int n_threads = 5;
int balance = 0;
int n_wrong_outputs = 0;

bool hasCorrectOutput(int balance, int n_deposits_iter) {
    int correct_balance = ammount_per_deposit * n_deposits_max;
    if (
        balance != correct_balance ||
        n_deposits_iter != n_deposits_max
    ) {
        n_wrong_outputs++;
        return false;
    }
    return true;
}

int main(int argc, char *argv[]) {
     using namespace std::chrono;

    // Check if user provided the correct number of args and then proceed, exit otherwise
    if (argc != 5 && argc != 1) {
        std::printf("Usage: %s <n-threads> <n-deposits> <ammount-per-deposit> <initial-balance>\n", argv[0]);
        std::printf("Or simply %s to use the default options\n", argv[0]);
        return EXIT_FAILURE;
    }
    if (argc == 5) {
        n_threads           = std::atoi(argv[1]);
        n_deposits_max      = std::atoi(argv[2]);
        ammount_per_deposit = std::atoi(argv[3]);
        balance             = std::atoi(argv[4]);
    }
    int n_deposits_iter = 0;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    omp_set_num_threads(n_threads);
        // std::printf("Id: %i, Block %i, Start %i, End %i\n", id, block, start, end);
        #pragma omp parallel for reduction (+:balance,n_deposits_iter)
        for (int i=n_deposits_iter; i<n_deposits_max; i++) {
            /*
            Because adding integers is very fast, purposefully slowing down this section
            will increase the chances of race condition as there's more 'time' in which
            any given two (or more) threads are in this section.
                
            It's also possible to expĺicitly split reading and adding 'balance' to make
            this race contition even more likely, this might require 
            turning OFF compile-time optimizations to be obverved.

            Example: slowing down using IO
            std::printf("Balance %i", balance);

            Example: spliting reading and adding:
            int tmp = balance
            <insert slow operation here>
            tmp += ammount_per_deposit
            balance = tmp;
            */
            // std::cout << omp_get_thread_num() << std::endl;
            n_deposits_iter++;
            balance += ammount_per_deposit;                
        }
        
    
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    hasCorrectOutput(balance, n_deposits_iter);
    std::cout << "Balance: " << balance << std::endl;
    std::cout << "N# of deposits: " << n_deposits_iter << std::endl;
    std::cout << "This run took " << time_span.count() << " seconds." << std::endl;
    std::cout << "N# wrong outputs: " << n_wrong_outputs << std::endl;
    return EXIT_SUCCESS;
}